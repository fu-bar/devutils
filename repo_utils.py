import os

from data.repos import working_folder
from helpers.git_helper import list_branches
from helpers.maven_helper import analyze_poms

if __name__ == '__main__':
    os.chdir(working_folder)
    analyze_poms(r'c:\temp\bmc_dependencies.xlsx')
    # git_clone()
    # checkout_branch('dev', False)
    # git_push()
    # maven_install()
    # git_pull()
    list_branches()
