import os

working_folder = r'c:\workspace\bmc'

git_repositories = {
        'rnd-hub@vs-ssh.visualstudio.com:v3/rnd-hub/ModularBMC/basic-bit-java-client',
        'rnd-hub@vs-ssh.visualstudio.com:v3/rnd-hub/ModularBMC/basic-skypicture-core-service',
        'rnd-hub@vs-ssh.visualstudio.com:v3/rnd-hub/ModularBMC/basic-skypicture-modules',
        'rnd-hub@vs-ssh.visualstudio.com:v3/rnd-hub/ModularBMC/bus-module',
        'rnd-hub@vs-ssh.visualstudio.com:v3/rnd-hub/ModularBMC/bus-shared',
        'rnd-hub@vs-ssh.visualstudio.com:v3/rnd-hub/ModularBMC/configuration-module',
        'rnd-hub@vs-ssh.visualstudio.com:v3/rnd-hub/ModularBMC/context-provider-module',
        'rnd-hub@vs-ssh.visualstudio.com:v3/rnd-hub/ModularBMC/ctlm-module',
        'rnd-hub@vs-ssh.visualstudio.com:v3/rnd-hub/ModularBMC/dds-channel-module',
        'rnd-hub@vs-ssh.visualstudio.com:v3/rnd-hub/ModularBMC/dmp-radar-dfs-core-service',
        'rnd-hub@vs-ssh.visualstudio.com:v3/rnd-hub/ModularBMC/dmp-radar-dfs-modules',
        'rnd-hub@vs-ssh.visualstudio.com:v3/rnd-hub/ModularBMC/elm',
        'rnd-hub@vs-ssh.visualstudio.com:v3/rnd-hub/ModularBMC/esm',
        'rnd-hub@vs-ssh.visualstudio.com:v3/rnd-hub/ModularBMC/mqtt-channel-module',
        'rnd-hub@vs-ssh.visualstudio.com:v3/rnd-hub/ModularBMC/notifications-module',
        'rnd-hub@vs-ssh.visualstudio.com:v3/rnd-hub/ModularBMC/osf-data',
        'rnd-hub@vs-ssh.visualstudio.com:v3/rnd-hub/ModularBMC/osf-refactor',
        'rnd-hub@vs-ssh.visualstudio.com:v3/rnd-hub/ModularBMC/osf-shared',
        'rnd-hub@vs-ssh.visualstudio.com:v3/rnd-hub/ModularBMC/osf-shared-infrastructure',
        'rnd-hub@vs-ssh.visualstudio.com:v3/rnd-hub/ModularBMC/osf-test-application',
        'rnd-hub@vs-ssh.visualstudio.com:v3/rnd-hub/ModularBMC/sgl',
        'rnd-hub@vs-ssh.visualstudio.com:v3/rnd-hub/ModularBMC/sgl-core-service',
        'rnd-hub@vs-ssh.visualstudio.com:v3/rnd-hub/ModularBMC/sgl-shared',
        'rnd-hub@vs-ssh.visualstudio.com:v3/rnd-hub/ModularBMC/sp-algorithm-stub',
        'rnd-hub@vs-ssh.visualstudio.com:v3/rnd-hub/ModularBMC/state-manager-module',
        'rnd-hub@vs-ssh.visualstudio.com:v3/rnd-hub/ModularBMC/synchronization-module',
        'rnd-hub@vs-ssh.visualstudio.com:v3/rnd-hub/ModularBMC/ui-backend-core-service',
        'rnd-hub@vs-ssh.visualstudio.com:v3/rnd-hub/ModularBMC/ui-backend-modules'
}

repo_folders = [os.path.join(working_folder, repo.split('/')[-1]) for repo in git_repositories]
