import glob
import os
import subprocess

import xlsxwriter
from pymaven.pom import Pom

from data.repos import repo_folders


def list_files(base_folder: str, file_suffix: str):
    for filename in glob.iglob(base_folder + '**/**', recursive=True):
        if not str(filename).endswith(file_suffix):
            continue
        yield filename


def analyze_poms(report_file_name: str):
    artifact_to_users = dict()
    artifact_to_dependencies = dict()

    for repo_folder in repo_folders:
        for pom_file in list_files(base_folder=repo_folder, file_suffix='pom.xml'):
            pom = Pom.fromstring(open(pom_file, 'r', encoding='utf8').read())
            for category in ['compile', 'test']:
                artifact_id = '{}::{}'.format(pom.artifact_id, pom.version)
                if artifact_id not in artifact_to_dependencies:
                    artifact_to_dependencies[artifact_id] = set()
                if category in pom.dependencies:
                    for dependency in pom.dependencies[category]:
                        # group = dependency[0][0]
                        name = dependency[0][1]
                        version = dependency[0][2]
                        dependency_id = '{}::{}'.format(name, version)
                        artifact_to_dependencies[artifact_id].add(dependency_id)
                        if dependency_id not in artifact_to_users:
                            artifact_to_users[dependency_id] = set()
                        artifact_to_users[dependency_id].add(pom.artifact_id)

    workbook = xlsxwriter.Workbook(report_file_name)

    for couple in [('artifact to dependencies', artifact_to_dependencies), ('artifact to user', artifact_to_users)]:
        sheet = workbook.add_worksheet(couple[0])
        column = 1
        for key in sorted(couple[1].keys()):
            sheet.write('A{}'.format(column), key)
            sheet.write('B{}'.format(column), ','.join(sorted(couple[1][key])))
            column += 1

    workbook.close()


def maven_install():
    for repo_folder in repo_folders:
        os.chdir(repo_folder)
        subprocess.run([r"c:\tools\apache-maven-3.6.3\bin\mvn.cmd", "-DskipTests=true", "clean", "install"])
