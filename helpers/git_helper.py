import os
import subprocess

import git

from data.repos import git_repositories
from data.repos import repo_folders
from data.repos import working_folder


def git_clone():
    os.chdir(working_folder)
    for repo in git_repositories:
        subprocess.run(["git", "clone", repo])


def checkout_branch(branch_name: str, new_branch: bool):
    for repo_folder in repo_folders:
        print(repo_folder)
        os.chdir(repo_folder)
        if new_branch:
            subprocess.run(["git", "checkout", "-b", branch_name])
        else:
            subprocess.run(["git", "checkout", branch_name])


def git_push(commit_message: str):
    for repo_folder in repo_folders:
        os.chdir(repo_folder)
        subprocess.run(["git", "add", "."])
        subprocess.run(["git", "commit", "-am", commit_message])
        subprocess.run(["git", "push", "origin", "osf_refactoring"])


def git_pull(branch_name: str):
    for repo_folder in repo_folders:
        os.chdir(repo_folder)
        subprocess.run(["git", "pull", "origin", branch_name])


def list_branches():
    for repo_folder in repo_folders:
        os.chdir(repo_folder)
        repo = git.Repo(search_parent_directories=False)
        branch = repo.active_branch
        print('{} --> {}'.format(branch, repo_folder))
